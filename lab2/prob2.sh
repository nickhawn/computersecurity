#!/bin/bash
set -u

min=0
max=2147483647

while (( $min <= $max )); do
  middle=$(( (min + max + 1 ) / 2 ))
  ./prob2 $middle
  test=$?
  echo $middle
  if [ $test == 0 ]
    then 
    echo "found at: "
    echo $middle
    exit 1
  fi

  if [ $test == 2 ]
    then
    min=$((middle+1))
  fi
  if [ $test == 1 ]
    then
    max=$((middle-1))
  fi
done