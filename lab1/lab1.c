#include <stdio.h>

void main(int argc, char * argv[], char * envp[]) {

  int variable;
  for (variable = 0; envp[variable] != NULL; variable++) {
    printf("\n%s", envp[variable]);
  }

  printf("\nShell's Value: %s\n", getenv("SHELL"));
  char * address = getenv("SHELL");
  printf("Shell's Address: %p\n", address);

}