#include <pcap.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include "./header.h"

void spoof_reply(struct ipheader* ip);
void send_raw_ip_packet(struct ipheader* ip);
void got_packet(u_char *args, const struct pcap_pkthdr *header, 
        const u_char *packet);

int main()
{
  pcap_t *handle;
  char errbuf[PCAP_ERRBUF_SIZE];
  struct bpf_program fp;
  char filter_exp[] = "ip proto icmp";
  bpf_u_int32 net = 0;

  // Step 1: Open live pcap session on NIC with name enp0s3 (ifconfig)
  handle = pcap_open_live("enp0s3", BUFSIZ, 1, 1000, errbuf); 
  if (handle == NULL) {
    printf("Cannot open live pcap session on NIC\n");
    exit(-1);
  }

  // Step 2: Compile filter_exp into BPF psuedo-code
  pcap_compile(handle, &fp, filter_exp, 0, net);      
  pcap_setfilter(handle, &fp);                             

  // Step 3: Capture packets
  pcap_loop(handle, -1, got_packet, NULL);                

  pcap_close(handle);   //Close the handle 
  return 0;
}

void got_packet(u_char *args, const struct pcap_pkthdr *header, 
        const u_char *packet) 
{
  struct ipheader *ip = (struct ipheader *)(packet + sizeof(struct ethheader));

  spoof_reply(ip);
}


/**********************************************
 * Listing 12.8: Spoofing a ICMP packet based on a captured ICMP packet and sending it
 **********************************************/
void spoof_reply(struct ipheader* ip)
{
    char buffer[1500];
    memset(buffer, 0, 1500); 

    struct icmpheader * icmp = (struct icmpheader *) (buffer + sizeof(struct ipheader));

    icmp->icmp_type = 8;

    icmp->icmp_chksum = 0;
    icmp->icmp_chksum = in_cksum((unsigned short *) icmp, sizeof(struct icmpheader));

    //struct ipheader *ip = (struct ipheader *) buffer;
    ip->iph_ver = 4;
    ip->iph_ihl = 5;
    ip->iph_ttl = 20;
    ip->iph_sourceip.s_addr = inet_addr("1.2.3.4");
    ip->iph_destip.s_addr = inet_addr("10.0.2.7");
    ip->iph_len = htons(sizeof(struct ipheader) + sizeof(struct icmpheader));
    
    struct sockaddr_in dest_info;
    int enable = 1;

    int sock = socket(AF_INET, SOCK_RAW, IPPROTO_RAW);

    setsockopt (sock, IPPROTO_IP, IP_HDRINCL, &enable, sizeof(enable));

    dest_info.sin_family = AF_INET;
    dest_info.sin_addr = ip->iph_destip;

    sendto(sock, ip, ntohs(ip->iph_len), 0, (struct sockaddr *)&dest_info, sizeof(dest_info));

    close(sock);
}
