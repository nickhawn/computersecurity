#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include "./header.h"

#define ETHER_ADDR_LEN

void got_packet(u_char *args, const struct pcap_pkthdr *header, 
        const u_char *packet)
{
  int i=0; 
    static int count=0; 
   printf("Got a packet\n");
   
   struct ipheader *ip = (struct ipheader *)(packet + sizeof(struct ethheader));
   printf("    From: %s\n", inet_ntoa(ip->iph_sourceip));
   printf("    To:   %s\n", inet_ntoa(ip->iph_destip));
   for(i=0;i<header->len;i++) { 
        if(isprint(packet[i]))                /* Check if the packet data is printable */
            printf("%c ",packet[i]);          /* Print it */
        else
            printf("",packet[i]);          /* If not print a . */
        if((i%16==0 && i!=0) || i==header->len-1) 
            printf(""); 
    }
}

int main()
{
  pcap_t *handle;
  char errbuf[PCAP_ERRBUF_SIZE];
  struct bpf_program fp;
  char filter_exp[] = "tcp";
  bpf_u_int32 net = 0;

  // Step 1: Open live pcap session on NIC with name enp0s3 (ifconfig)
  handle = pcap_open_live("enp0s3", BUFSIZ, 1, 1000, errbuf); 

  if (handle == NULL) {
    printf("Cannot open pcap live!\n");
    exit(-1);
  }

  // Step 2: Compile filter_exp into BPF psuedo-code
  pcap_compile(handle, &fp, filter_exp, 0, net);      
  pcap_setfilter(handle, &fp);                             

  // Step 3: Capture packets
  pcap_loop(handle, -1, got_packet, NULL);                

  pcap_close(handle);   //Close the handle 
  return 0;
}
