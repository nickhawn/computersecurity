/*Vunlerable program: stack.c*/
/*You can get this program from the lab’s website*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
/*Changing this size will change the layout of the stack.*Instructors can 
change this value each year, so students*won’t be able to use the solutions 
from the past.*Suggested value: between 0 and 400*/


int bof(char*str){
char buffer[100];

/*The following statement has a buffer overflow problem*/

strcpy(buffer, str);
return 1;

}

int main(int argc, char**argv){
	char str[100];
	FILE *badfile;

	badfile = fopen("badfile", "r");
	fread(str, sizeof(char), 300, badfile);
	bof(str);
	printf("Returned Properly\n");
	return 1;
}